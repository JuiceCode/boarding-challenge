<?php
declare(strict_types=1);

namespace TripSorter\Journey;

use TripSorter\Boarding\Boarding;
use TripSorter\BoardingSorter\BoardingSorter;

class Journey
{
    /** @var BoardingSorter */
    private $boardingSorter;
    /** @var Boarding[] */
    private $boardings;

    public function __construct(BoardingSorter $boardingSorter)
    {
        $this->boardingSorter = $boardingSorter;
    }

    public function addBoarding(Boarding $boarding): void
    {
        $this->boardings[] = $boarding;
    }

    /**
     * @return Boarding[]
     */
    public function getSorted(): array
    {
        return $this->boardingSorter->sort($this->boardings);
    }
}
