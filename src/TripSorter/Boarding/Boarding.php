<?php
declare(strict_types=1);

namespace TripSorter\Boarding;

use TripSorter\City\City;

interface Boarding
{
    public function getDeparture(): City;

    public function getDestination(): City;
}
