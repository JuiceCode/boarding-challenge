<?php
declare(strict_types=1);

namespace TripSorter\Boarding\Train;

use TripSorter\Boarding\Boarding;
use TripSorter\Boarding\ID;
use TripSorter\City\City;

class Train implements Boarding
{
    /** @var City */
    private $departure;
    /** @var City */
    private $destination;
    /** @var ID */
    private $id;
    /** @var string */
    private $seat;

    public function __construct(City $departure, City $destination, ID $id, string $seat)
    {

        $this->departure   = $departure;
        $this->destination = $destination;
        $this->id          = $id;
        $this->seat        = $seat;
    }

    public function getDeparture(): City
    {
        return $this->departure;
    }

    public function getDestination(): City
    {
        return $this->destination;
    }

    public function getId(): ID
    {
        return $this->id;
    }

    public function getSeat(): string
    {
        return $this->seat;
    }
}
