<?php
declare(strict_types=1);

namespace TripSorter\Boarding\Flight;

use TripSorter\Boarding\Boarding;
use TripSorter\Boarding\ID;
use TripSorter\City\City;

class Flight implements Boarding
{
    /** @var City */
    private $departure;
    /** @var City */
    private $destination;
    /** @var ID */
    private $id;
    /** @var Gate */
    private $gate;
    /** @var string */
    private $seat;
    /** @var Baggage */
    private $baggage;

    public function __construct(City $departure, City $destination, ID $id, Gate $gate, string $seat, Baggage $baggage)
    {
        $this->departure   = $departure;
        $this->destination = $destination;
        $this->id          = $id;
        $this->gate        = $gate;
        $this->seat        = $seat;
        $this->baggage     = $baggage;
    }

    public function getDeparture(): City
    {
        return $this->departure;
    }

    public function getDestination(): City
    {
        return $this->destination;
    }

    public function getId(): ID
    {
        return $this->id;
    }

    public function getGate(): Gate
    {
        return $this->gate;
    }

    public function getSeat(): string
    {
        return $this->seat;
    }

    public function getBaggage(): Baggage
    {
        return $this->baggage;
    }
}
