<?php
declare(strict_types=1);

namespace TripSorter\Boarding\Flight;

class Gate
{
    /** @var string */
    private $number;

    public function __construct(string $number)
    {
        $this->number = $number;
    }

    public function getNumber(): string
    {
        return $this->number;
    }
}
