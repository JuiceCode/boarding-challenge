<?php
declare(strict_types=1);

namespace TripSorter\Boarding\Flight;


class Baggage
{
    /** @var bool */
    private $automaticallyTransfer = false;

    /** @var string */
    private $dropAt;

    public static function automaticallyTransfer(): self
    {
        $baggage = new self();

        $baggage->automaticallyTransfer = true;

        return $baggage;
    }

    public static function dropAt(string $dropAt): self
    {
        $baggage = new self();

        $baggage->dropAt = $dropAt;

        return $baggage;
    }

    public function isAutomaticallyTransfer(): bool
    {
        return $this->automaticallyTransfer;
    }

    public function getDropAt(): string
    {
        return $this->dropAt;
    }
}
