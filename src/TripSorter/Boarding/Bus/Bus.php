<?php

namespace TripSorter\Boarding\Bus;

use TripSorter\Boarding\Boarding;
use TripSorter\City\City;

class Bus implements Boarding
{
    /** @var City */
    private $departure;
    /** @var City */
    private $destination;

    public function __construct(City $departure, City $destination)
    {
        $this->departure   = $departure;
        $this->destination = $destination;
    }

    public function getDeparture(): City
    {
        return $this->departure;
    }

    public function getDestination(): City
    {
        return $this->destination;
    }
}
