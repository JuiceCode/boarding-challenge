<?php
declare(strict_types=1);

namespace TripSorter\City;

class City
{
    /** @var string */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function equals(self $city): bool
    {
        return $this->getName() === $city->getName();
    }
}
