<?php
declare(strict_types=1);

namespace TripSorter\BoardingSorter;


use TripSorter\Boarding\Boarding;

interface BoardingSorter
{
    /**
     * @param Boarding[] $boardings
     * @return Boarding[]
     */
    public function sort(array $boardings): array;
}
