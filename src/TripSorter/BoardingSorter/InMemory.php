<?php
declare(strict_types=1);

namespace TripSorter\BoardingSorter;

use TripSorter\Boarding\Boarding;
use TripSorter\BoardingSorter\Exception\MissingJourneyBeginningException;
use TripSorter\BoardingSorter\Exception\NoForwardConnectionException;
use TripSorter\City\City;

class InMemory implements BoardingSorter
{
    /** @var Connection[] */
    private $connections;
    /** @var SwitchPoint[] */
    private $switchPoints;

    /**
     * @param Boarding[] $boardings
     * @return Boarding[]
     * @throws MissingJourneyBeginningException
     * @throws NoForwardConnectionException
     */
    public function sort(array $boardings): array
    {
        foreach ($boardings as $boarding) {
            $connection = $this->addConnection($boarding->getDeparture(), $boarding->getDestination(), $boarding);
            $this->addSwitchPoint($boarding->getDeparture(), $connection);
            $this->addSwitchPoint($boarding->getDestination(), $connection);
        }

        $boardings = [];

        $firstAndLastNodes = $this->filterSwitchPointsByConnectionsAmount($this->switchPoints, 2);
        $predecessor       = $this->firstSwitchPointFinder($firstAndLastNodes);

        $count = count($this->switchPoints);

        while (count($boardings) !== $count - 1) {
            $connection  = $this->forwardConnectionFilter($predecessor->getConnections(), $predecessor->getCity());
            $boardings[] = $connection->getBoarding();
            $predecessor = $this->switchPoints[$connection->getTo()->getName()];
        }

        return $boardings;
    }

    private function addConnection(City $from, City $to, Boarding $boarding): Connection
    {
        $connection          = new Connection($from, $to, $boarding);
        $this->connections[] = $connection;

        return $connection;
    }

    private function addSwitchPoint(City $city, ?Connection $connection): SwitchPoint
    {

        if (isset($this->switchPoints[$city->getName()])) {
            $switchPoint = $this->switchPoints[$city->getName()]->addConnection($connection);
        } else {
            $switchPoint                          = (new SwitchPoint($city))->addConnection($connection);
            $this->switchPoints[$city->getName()] = $switchPoint;
        }

        return $switchPoint;
    }

    /**
     * @param SwitchPoint[] $switchPoints
     * @param int $connectionsAmount
     * @return SwitchPoint[]
     */
    private function filterSwitchPointsByConnectionsAmount(array $switchPoints, int $connectionsAmount): array
    {
        return array_filter(
            $switchPoints,
            function (SwitchPoint $switchPoint) use ($connectionsAmount): bool {
                return count($switchPoint->getConnections()) < $connectionsAmount;
            }
        );
    }

    /**
     * @param SwitchPoint[] $switchPoints
     * @return SwitchPoint
     * @throws MissingJourneyBeginningException
     */
    private function firstSwitchPointFinder(array $switchPoints): SwitchPoint
    {
        foreach ($switchPoints as $switchPoint) {
            if ($switchPoint->getConnections()[0]->getFrom()->equals($switchPoint->getCity())) {
                return $switchPoint;
            }
        }

        throw new MissingJourneyBeginningException();
    }

    /**
     * @param Connection[] $connections
     * @param City $from
     * @return Connection
     * @throws NoForwardConnectionException
     */
    private function forwardConnectionFilter(array $connections, City $from): Connection
    {
        foreach ($connections as $connection) {
            if ($connection->getFrom()->equals($from)) {
                return $connection;
            }
        }

        throw new NoForwardConnectionException();
    }
}
