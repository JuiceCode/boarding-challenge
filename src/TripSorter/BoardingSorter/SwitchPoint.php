<?php
declare(strict_types=1);

namespace TripSorter\BoardingSorter;

use TripSorter\City\City;

class SwitchPoint
{
    /** @var City */
    private $city;
    /** @var Connection[] */
    private $connections;

    public function __construct(City $city)
    {
        $this->city        = $city;
        $this->connections = [];
    }

    public function getCity(): City
    {
        return $this->city;
    }

    /**
     * @return Connection[]
     */
    public function getConnections(): array
    {
        return $this->connections;
    }

    public function addConnection(?Connection $connection): SwitchPoint
    {
        $this->connections[] = $connection;

        return $this;
    }
}
