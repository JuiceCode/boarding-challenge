<?php
declare(strict_types=1);

namespace TripSorter\BoardingSorter;

use TripSorter\Boarding\Boarding;
use TripSorter\City\City;

class Connection
{
    /** @var City */
    private $from;
    /** @var City */
    private $to;
    /** @var Boarding */
    private $boarding;

    public function __construct(City $from, City $to, Boarding $boarding)
    {
        $this->from     = $from;
        $this->to       = $to;
        $this->boarding = $boarding;
    }

    public function getFrom(): City
    {
        return $this->from;
    }

    public function getTo(): City
    {
        return $this->to;
    }

    public function getBoarding(): Boarding
    {
        return $this->boarding;
    }
}
