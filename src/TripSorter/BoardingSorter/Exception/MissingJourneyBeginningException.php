<?php
declare(strict_types=1);

namespace TripSorter\BoardingSorter\Exception;

use TripSorter\Exception\TripSorterException;

class MissingJourneyBeginningException extends TripSorterException
{
    public function __construct()
    {
        parent::__construct('Missing point that could be start for a journey');
    }
}
