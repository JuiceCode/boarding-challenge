<?php
declare(strict_types=1);

namespace TripSorter\BoardingSorter\Exception;

use TripSorter\Exception\TripSorterException;

class NoForwardConnectionException extends TripSorterException
{
    public function __construct()
    {
        parent::__construct('Missing forward connection');
    }
}
