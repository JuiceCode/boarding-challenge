<?php
declare(strict_types=1);

namespace Tests\Functional;

use PHPUnit\Framework\TestCase;
use TripSorter\Boarding\Boarding;
use TripSorter\Boarding\Bus\Bus;
use TripSorter\Boarding\Flight\Baggage;
use TripSorter\Boarding\Flight\Flight;
use TripSorter\Boarding\Flight\Gate;
use TripSorter\Boarding\ID;
use TripSorter\Boarding\Train\Train;
use TripSorter\BoardingSorter\InMemory;
use TripSorter\City\City;
use TripSorter\Journey\Journey;

class JourneySorterTest extends TestCase
{
    private const WARSAW  = 'Warsaw';
    private const BERLIN  = 'Berlin';
    private const BEIJING = 'Beijing';
    private const NY      = 'NY';
    private const DUBAI   = 'Dubai';
    private const BANGKOK = 'Bangkok';
    private const PARIS   = 'Paris';

    /**
     * @test
     */
    public function it_should_return_sorted_journey()
    {
        $warsaw  = new City(self::WARSAW);
        $berlin  = new City(self::BERLIN);
        $beijing = new City(self::BEIJING);
        $ny      = new City(self::NY);
        $dubai   = new City(self::DUBAI);
        $bangkok = new City(self::BANGKOK);
        $paris   = new City(self::PARIS);

        $boardings = [
            new Bus($beijing, $bangkok),
            new Flight($berlin, $ny, new ID('tttt'), new Gate('34A'), '3a1', Baggage::automaticallyTransfer()),
            new Flight($bangkok, $paris, new ID('45ADT'), new Gate('333'), '111', Baggage::dropAt('123')),
            new Bus($dubai, $beijing),
            new Bus($ny, $dubai),
            new Train($warsaw, $berlin, new ID('12345'), '12E'),
        ];

        //warsaw -> berlin -> ny -> dubai -> beijing -> bangkok

        $journey = new Journey(new InMemory());
        foreach ($boardings as $boarding) {
            $journey->addBoarding($boarding);
        }

        $sortedBoardings = $journey->getSorted();

        /** @var Boarding[] $sortedBoardings */
        $this->assertSame(self::WARSAW, $sortedBoardings[0]->getDeparture()->getName());
        $this->assertSame(self::BERLIN, $sortedBoardings[0]->getDestination()->getName());
        $this->assertInstanceOf(Train::class, $sortedBoardings[0]);

        $this->assertSame(self::BERLIN, $sortedBoardings[1]->getDeparture()->getName());
        $this->assertSame(self::NY, $sortedBoardings[1]->getDestination()->getName());
        $this->assertInstanceOf(Flight::class, $sortedBoardings[1]);

        $this->assertSame(self::NY, $sortedBoardings[2]->getDeparture()->getName());
        $this->assertSame(self::DUBAI, $sortedBoardings[2]->getDestination()->getName());
        $this->assertInstanceOf(Bus::class, $sortedBoardings[2]);

        $this->assertSame(self::DUBAI, $sortedBoardings[3]->getDeparture()->getName());
        $this->assertSame(self::BEIJING, $sortedBoardings[3]->getDestination()->getName());
        $this->assertInstanceOf(Bus::class, $sortedBoardings[3]);

        $this->assertSame(self::BEIJING, $sortedBoardings[4]->getDeparture()->getName());
        $this->assertSame(self::BANGKOK, $sortedBoardings[4]->getDestination()->getName());
        $this->assertInstanceOf(Bus::class, $sortedBoardings[4]);

        $this->assertSame(self::BANGKOK, $sortedBoardings[5]->getDeparture()->getName());
        $this->assertSame(self::PARIS, $sortedBoardings[5]->getDestination()->getName());
        $this->assertInstanceOf(Flight::class, $sortedBoardings[5]);

    }
}
