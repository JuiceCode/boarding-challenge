init:
	docker-compose run --rm php-cli php composer.phar install

test-cli:
	docker-compose run --rm php-cli bash

specs:
	docker-compose run --rm php-cli ./bin/phpspec run --format=dot --no-code-generation

test:
	docker-compose run --rm php-cli ./bin/phpunit tests/
