<?php
declare(strict_types=1);

namespace spec\TripSorter\City;

use PhpSpec\ObjectBehavior;
use TripSorter\City\City;

class CitySpec extends ObjectBehavior
{
    public function it_should_be_constructed_with_city_name()
    {
        $cityName = 'Barcelona';
        $this->beConstructedWith($cityName);
        $this->getName()->shouldBe($cityName);
    }

    public function it_should_equals_to_same_city(City $city)
    {
        $cityName = 'Madrid';
        $city->getName()->willReturn($cityName);
        $this->beConstructedWith($cityName);
        $this->equals($city)->shouldReturn(true);
    }

    public function it_should_not_equals_to_different_city(City $city)
    {
        $madrid = 'Madrid';
        $newYork = 'New York';
        $city->getName()->willReturn($newYork);
        $this->beConstructedWith($madrid);
        $this->equals($city)->shouldReturn(false);
    }
}
