<?php
declare(strict_types=1);

namespace spec\TripSorter\Journey;

use PhpSpec\ObjectBehavior;
use TripSorter\Boarding\Boarding;
use TripSorter\BoardingSorter\BoardingSorter;
use TripSorter\Journey\Journey;

/**
 * @mixin Journey
 */
class JourneySpec extends ObjectBehavior
{
    public function it_should_add_boarding(BoardingSorter $boardingSorter, Boarding $boarding)
    {
        $this->beConstructedWith($boardingSorter);
        $this->addBoarding($boarding);
        $boardingSorter->sort([$boarding])->shouldBecalled()->willReturn([$boarding]);
        $this->getSorted()->shouldBe([$boarding]);
    }
}
