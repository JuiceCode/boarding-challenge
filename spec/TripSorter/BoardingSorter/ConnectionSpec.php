<?php
declare(strict_types=1);

namespace spec\TripSorter\BoardingSorter;

use PhpSpec\ObjectBehavior;
use TripSorter\Boarding\Boarding;
use TripSorter\City\City;

class ConnectionSpec extends ObjectBehavior
{
    public function it_should_be_proper_connection(City $from, City $to, Boarding $boarding)
    {
        $this->beConstructedWith($from, $to, $boarding);
        $this->getFrom()->shouldBe($from);
        $this->getTo()->shouldBe($to);
        $this->getBoarding()->shouldBe($boarding);
    }
}
