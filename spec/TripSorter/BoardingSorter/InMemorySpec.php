<?php
declare(strict_types=1);

namespace spec\TripSorter\BoardingSorter;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use TripSorter\Boarding\Boarding;
use TripSorter\BoardingSorter\InMemory;
use TripSorter\City\City;

/**
 * @mixin InMemory
 */
class InMemorySpec extends ObjectBehavior
{
    public function it_should_return_sorted_boardings(
        Boarding $boarding1,
        Boarding $boarding2,
        Boarding $boarding3,
        Boarding $boarding4,
        City $city1,
        City $city2,
        City $city3,
        City $city4,
        City $city5
    ) {
        $city1->getName()->willReturn('1');
        $city2->getName()->willReturn('2');
        $city3->getName()->willReturn('3');
        $city4->getName()->willReturn('4');
        $city5->getName()->willReturn('5');

        $city1->equals($city1)->willReturn(true);
        $city1->equals(Argument::type(City::class))->willReturn(false);
        $city2->equals($city2)->willReturn(true);
        $city2->equals(Argument::type(City::class))->willReturn(false);
        $city3->equals($city3)->willReturn(true);
        $city3->equals(Argument::type(City::class))->willReturn(false);
        $city4->equals($city4)->willReturn(true);
        $city4->equals(Argument::type(City::class))->willReturn(false);
        $city5->equals($city5)->willReturn(true);
        $city5->equals(Argument::type(City::class))->willReturn();

        $boarding1->getDeparture()->willReturn($city1);
        $boarding1->getDestination()->willReturn($city2);

        $boarding2->getDeparture()->willReturn($city2);
        $boarding2->getDestination()->willReturn($city3);

        $boarding3->getDeparture()->willReturn($city3);
        $boarding3->getDestination()->willReturn($city4);

        $boarding4->getDeparture()->willReturn($city4);
        $boarding4->getDestination()->willReturn($city5);

        $this->sort([$boarding4, $boarding2, $boarding3, $boarding1])->shouldBe(
            [
                $boarding1,
                $boarding2,
                $boarding3,
                $boarding4,
            ]
        );
    }
}
