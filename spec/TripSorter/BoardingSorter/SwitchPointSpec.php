<?php
declare(strict_types=1);

namespace spec\TripSorter\BoardingSorter;


use PhpSpec\ObjectBehavior;
use TripSorter\BoardingSorter\Connection;
use TripSorter\City\City;

class SwitchPointSpec extends ObjectBehavior
{
    public function let(City $city)
    {
        $this->beConstructedWith($city);
    }

    public function it_should_be_proper_switch_point(City $city)
    {
        $this->getCity()->shouldBe($city);
        $this->getConnections()->shouldBe([]);
    }

    public function it_should_add_connections(Connection $connection1, Connection $connection2)
    {
        $this->addConnection($connection1);
        $this->addConnection($connection2);

        $this->getConnections()->shouldBe([$connection1, $connection2]);
    }
}
