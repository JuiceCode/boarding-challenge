<?php
declare(strict_types=1);

namespace spec\TripSorter\Boarding\Flight;

use PhpSpec\ObjectBehavior;
use TripSorter\Boarding\Flight\Baggage;
use TripSorter\Boarding\Flight\Gate;
use TripSorter\Boarding\ID;
use TripSorter\City\City;

class FlightSpec extends ObjectBehavior
{
    public function it_should_be_correct_flight_data(
        City $departure,
        City $destination,
        ID $flightNumber,
        Gate $gate,
        Baggage $baggage
    ) {
        $this->beConstructedWith($departure, $destination, $flightNumber, $gate, '3A', $baggage);
        $this->getDeparture()->shouldBe($departure);
        $this->getDestination()->shouldBe($destination);
        $this->getId()->shouldBe($flightNumber);
        $this->getGate()->shouldBe($gate);
        $this->getSeat()->shouldBe('3A');
        $this->getBaggage()->shouldBe($baggage);
    }
}

