<?php
declare(strict_types=1);

namespace spec\TripSorter\Boarding\Flight;

use PhpSpec\ObjectBehavior;

class BaggageSpec extends ObjectBehavior
{
    public function it_should_be_automatic_transfer()
    {
        $this->beConstructedThrough('automaticallyTransfer');
        $this->isAutomaticallyTransfer()->shouldBe(true);
    }

    public function it_should_be_baggage_dropped_at_ticket_counter()
    {
        $this->beConstructedThrough('dropAt', ['344']);
        $this->isAutomaticallyTransfer()->shouldBe(false);
        $this->getDropAt()->shouldBe('344');
    }
}
