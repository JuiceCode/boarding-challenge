<?php
declare(strict_types=1);

namespace spec\TripSorter\Boarding\Flight;

use PhpSpec\ObjectBehavior;

class GateSpec extends ObjectBehavior
{
    public function it_should_return_gate_number()
    {
        $this->beConstructedWith('34B');
        $this->getNumber()->shouldBe('34B');
    }
}
