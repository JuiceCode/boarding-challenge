<?php
declare(strict_types=1);

namespace spec\TripSorter\Boarding\Train;

use PhpSpec\ObjectBehavior;
use TripSorter\Boarding\ID;
use TripSorter\City\City;

class TrainSpec extends ObjectBehavior
{
    public function it_should_be_proper_train_data(
        City $departure,
        City $destination,
        ID $trainNumber
    ) {
        $seat = '45B';
        $this->beConstructedWith($departure, $destination, $trainNumber, $seat);
        $this->getDeparture()->shouldBe($departure);
        $this->getDestination()->shouldBe($destination);
        $this->getId()->shouldBe($trainNumber);
        $this->getSeat()->shouldBe($seat);
    }
}
