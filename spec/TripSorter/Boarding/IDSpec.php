<?php
declare(strict_types=1);

namespace spec\TripSorter\Boarding;

use PhpSpec\ObjectBehavior;

class IDSpec extends ObjectBehavior
{
    public function it_should_return_flight_number()
    {
        $flightNumber = 'SK455';
        $this->beConstructedWith($flightNumber);

        $this->getNumber()->shouldBe($flightNumber);
    }
}
