<?php
declare(strict_types=1);

namespace spec\TripSorter\Boarding\Bus;

use PhpSpec\ObjectBehavior;
use TripSorter\City\City;

class BusSpec extends ObjectBehavior
{
    public function it_should_be_proper_bus_data(City $departure, City $destination)
    {
        $this->beConstructedWith($departure, $destination);
        $this->getDeparture()->shouldBe($departure);
        $this->getDestination()->shouldBe($destination);
    }
}
