#TripSorter

## Requirements

* Docker 18.09

## Tests

Before tests you need to run 
```bash
make init 
```

To run functional tests
```bash
make test
```

To run unit tests
```bash
make specs
```

## How to use it
Create cities that will indicate departure and destination of boarding
```php
$berlin = new City('Berlin');
$ny = new City('NewYork');
```

Create boardings
```php
$flight = new Flight($berlin, $ny, new ID('QQ1234'), new Gate('34A'), '7B', Baggage::automaticallyTransfer());
$flight = new Flight($berlin, $ny, new ID('QQ1234'), new Gate('34A'), '7B', Baggage::dropAt('344'));
$train = new Train($berlin, $ny, new ID('12952'), '12E');
$bus = new Bus($berlin, $dubai);
```

Create journey with selected sorter (for now there is only in memory sorter)
```php
$journey = new Journey(InMemory());
```

Add boardings to Journey
```php
$journey->addBoarding($flight);
$journey->addBoarding($train);
$journey->addBoarding($bus);
```

This function returns sorted boardings form the beginning to the end
```php
$jourey->getSorted();
```
